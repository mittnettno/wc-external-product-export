<?php
/**
 * Plugin Name: WC External Product Export
 * Author: Bjørn Kristiansen & Morten Kristiansen - InHouse AS
 * Version: 0.0.1
 */
	
	add_filter( 'woocommerce_product_export_product_column_type', 'jai_set_product_to_external', 10, 2 );
	function jai_set_product_to_external($value, $product) {
		$value = "external";
		return $value;
	}

	add_filter( 'woocommerce_product_export_product_column_product_url', 'jai_set_external_product_url', 10, 2 );
	function jai_set_external_product_url($value, $product) {
		$value = get_permalink($product->get_id());
		return $value;
	}

	//add_filter( 'woocommerce_product_export_row_data', 'jai_unset_variants', 10, 2 );
	function jai_unset_variants($row,$product){
		if($product->is_type( 'variation'))
			unset($row);
		return $row;
	}

	add_filter( 'woocommerce_product_export_product_query_args', 'jai_set_product_types' );
	function jai_set_product_types($args) {
		$args['type'] = array('simple', 'variable');
		return $args;
	}