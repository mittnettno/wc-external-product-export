# README #

Simple WordPress drop in plugin to export WooCommerce products as external products.



### What is this repository for? ###

* Super simple plugin to make the WC product export create a csv file where simple and variable products are exported as external products
* Version 0.0.1

### How do I get set up? ###

* Just install and activate

### Contribution guidelines ###

* Make it better!

### Who do I talk to? ###

* No support given :-)